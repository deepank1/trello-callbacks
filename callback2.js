//function that will return all lists that belong to a board 
//based on the boardID that is passed to it from the given data in lists.json. 
//Then pass control back to the code that called it by using a callback function.

const fs = require("fs")
const path = require("path")

function callback2(boardID, callback) {
    setTimeout(() => {
        getListDetails(boardID, callback)
    }, 2 * 1000)
}

function getListDetails(boardID, callback) {

    let listFileName = "lists.json"

    fs.readFile(path.join(__dirname, listFileName), (err, data) => {
        if (err) {
            callback(err)
        }
        else {
            let listDetails = JSON.parse(data)
            let allList = listDetails[boardID]
            if (allList != undefined) {
                callback(null, allList)
            }
            else {
                callback("No List for this Board ID")
            }
        }
    })
}

module.exports = callback2