//function that will use the previously written functions to get the following information. 
//You do not need to pass control back to the code that called it.

// Get information from the Thanos boards
// Get all the lists for the Thanos board
// Get all cards for the Mind and Space lists simultaneously



const fs = require("fs")
const path = require("path")

const getBoardDetails = require("./callback1")
const getListDetails = require("./callback2")
const getCardDetails = require("./callback3")


function callback5(boardName, cardsOf1, cardsOf2, callback) {
    setTimeout(() => {
        getAllDetails(boardName, cardsOf1, cardsOf2, callback)
    }, 2 * 1000)
}

function getAllDetails(boardName, cardsOf1, cardsOf2, callback) {

    let boardFile = "boards.json"
    fs.readFile(path.join(__dirname, boardFile), (err, data) => {
        if (err) {
            callback(err)
        }
        else {
            let boardArray = JSON.parse(data)
            let boardFound = 0

            for (let index = 0; index < boardArray.length; index++) {
                if (boardArray[index].name.toLowerCase() === boardName.toLowerCase()) {
                    boardID = boardArray[index].id
                    boardFound = 1
                    getBoardDetails(boardID, (err, data) => {
                        if (err) {
                            callback(err)
                        }
                        else {
                            callback(null, data)
                            getListDetails(boardID, (err, data) => {
                                if (err) {
                                    callback(err)
                                }
                                else {
                                    callback(null, data)
                                    let listDetails = data
                                    let cardID = 0
                                    let card1Found = 0
                                    let card2Found = 0

                                    for (let index = 0; index < listDetails.length; index++) {
                                        if (listDetails[index].name.toLowerCase() === cardsOf1.toLowerCase()) {
                                            cardID = listDetails[index].id
                                            getCardDetails(cardID, (err, data) => {
                                                if (err) {
                                                    callback(err)
                                                }
                                                else {
                                                    callback(data)
                                                    card1Found = 1
                                                }
                                            })

                                        }
                                        if (listDetails[index].name.toLowerCase() === cardsOf2.toLowerCase()) {
                                            cardID = listDetails[index].id
                                            getCardDetails(cardID, (err, data) => {
                                                if (err) {
                                                    callback(err)
                                                }
                                                else {
                                                    callback(data)
                                                    card2Found = 1
                                                }
                                            })
                                        }
                                        if (card1Found + card2Found == 2) {
                                            break
                                        }
                                    }

                                }


                            })
                        }
                    })
                }

            }
            if(boardFound==0){
                callback("No board info for this board Name")
            }
        }
    })
}

module.exports = callback5