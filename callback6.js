//function that will use the previously written functions to get the following information. 
//You do not need to pass control back to the code that called it.

// Get information from the Thanos boards
// Get all the lists for the Thanos board
// Get all cards for all lists simultaneously


const fs = require("fs")
const path = require("path")

const getBoardDetails = require("./callback1")
const getListDetails = require("./callback2")
const getCardDetails = require("./callback3")


function callback6(boardID, callback) {
    setTimeout(() => {
        getAllDetails(boardID, callback)
    }, 2 * 1000)
}

function getAllDetails(boardID, callback) {

    let boardFile = "boards.json"
    fs.readFile(path.join(__dirname, boardFile), (err, data) => {
        if (err) {
            callback(err)
        }
        else {
            let boardArray = JSON.parse(data)
            let boardFound = 0

            for (let index = 0; index < boardArray.length; index++) {
                if (boardArray[index].name.toLowerCase() === boardName.toLowerCase()) {
                    boardID = boardArray[index].id
                    boardFound = 1
                    getBoardDetails(boardID, (err, data) => {
                        if (err) {
                            callback(err)
                        }
                        else {
                            callback(null, data)
                            getListDetails(boardID, (err, data) => {
                                if (err) {
                                    callback(err)
                                }
                                else {
                                    callback(null, data)
                                    let listDetails = data
                                    for (let index = 0; index < listDetails.length; index++) {
                                        getCardDetails(listDetails[index].id, (err, data) => {
                                            if (err) {
                                                callback(err)
                                            }
                                            else {
                                                if (data !== undefined) {
                                                    callback(null, data)
                                                }

                                            }
                                        })

                                    }
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

module.exports = callback6