//function that will return all cards that belong to a particular list 
//based on the listID that is passed to it from the given data in cards.json. 
//Then pass control back to the code that called it by using a callback function.

const fs = require("fs")
const path = require("path")


function callback3(listId, callback) {
    setTimeout(() => {
        getCardDetails(listId, callback)
    }, 2 * 1000)
}

function getCardDetails(listId, callback) {
    const cardFilename = "cards.json"

    fs.readFile(path.join(__dirname, cardFilename), (err, data) => {
        if (err) {
            callback(err)
        }
        else {
            let cardDetails = JSON.parse(data)
            let allcards = cardDetails[listId]
            if (allcards != undefined) {
                callback(null, allcards)
            }
            else {
                callback("No Card for this List ID")
            }
        }
    })
}

module.exports = callback3