const fs = require("fs")
const path = require("path")


function callback1(idNum, callback) {
    setTimeout(()=>{
        getBoardDetails(idNum,callback)
    }, 2*1000)
}

function getBoardDetails(idNum, callback) {
    const boardsFileName = "boards.json"

    fs.readFile(path.join(__dirname,boardsFileName), (err,data)=>{
        if(err){
            callback(err)
        }
        else{
            let boardArray = JSON.parse(data)
            let boardFound = 0 

            for(let index=0; index < boardArray.length; index++){
                if(boardArray[index].id == idNum){
                    callback(null,boardArray[index])
                    boardFound = 1
                    break
                }
            }
            if(boardFound==0){
                callback("Invalid BoardId")
            }
            
        }
    })
}



module.exports = callback1